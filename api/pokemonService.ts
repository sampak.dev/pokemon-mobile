import { useInfiniteQuery, useQuery } from "react-query";
import { IPokemonListResponse } from "../dto/response/IPokemonList.response";
import { axiosInstance } from "./axios"

const queryKeys = {
  getList: ['pokemonService.getList'],
  getPokemon: (pokemonId: string) => ['pokemonService.getList', pokemonId]
}

const getPokemon = async (pokemonId: string) => {
  const { data } = await axiosInstance.get(`/pokemon/${pokemonId}`);
  return data ?? []
}

const useGetPokemon = (pokemonId: string) => {
  return useQuery(queryKeys.getPokemon(pokemonId), () => getPokemon(pokemonId))
}

const getList = async ({pageParam} : {pageParam?: string | undefined}): Promise<IPokemonListResponse> => {
  const { data } = await axiosInstance.get(pageParam ? pageParam : '/pokemon?limit=20');
  return data ?? [];
}

const useGetList = () => {
  return useInfiniteQuery(queryKeys.getList, getList, {
      getNextPageParam: (lastPage) => {
        if(!lastPage.next) return undefined;
        return lastPage.next;
      },
  })
}


export default {
  useGetPokemon,
  useGetList
}