import axios from 'axios';

export const axiosInstance = axios.create({
  baseURL: process.env.EXPO_PUBLIC_API_URL ?? 'http://localhost:3000',
  headers: {
    'content-type': 'application/json',
  },
});

// const reqInterceptor = (config) => {
//   if (!config.headers) {
//     config.headers = {};
//   }



//   return config;
// };

// const reqInt = axiosInstance.interceptors.request.use(reqInterceptor);
