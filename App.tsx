import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import PokemonList from './views/PokemonList';
import { QueryClient, QueryClientProvider } from 'react-query';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import PokemonDetails from './views/PokemonDetails';
import { EViews } from './dto/EViews';

export type RootStackParamList = {
  PokemonList: undefined;
  PokemonDetails: { pokemonId: string };
};

const queryClient = new QueryClient();
const Stack = createStackNavigator<RootStackParamList>();


export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName={EViews.POKEMON_LIST}>

          <Stack.Screen options={
            {
              headerShown: false
            }
          } name={EViews.POKEMON_LIST} component={PokemonList} />
          
          <Stack.Screen 
            options={{ title: 'Szczegóły Pokemona' }}  
            name={EViews.POKEMON_DETAILS}          
            component={PokemonDetails} 
          />
        </Stack.Navigator>
      </NavigationContainer>
      <StatusBar style="auto" />
    </QueryClientProvider>
  );
}


