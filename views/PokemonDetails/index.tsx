import { RouteProp } from "@react-navigation/native";
import { StyleSheet, View, Text,Image, ActivityIndicator } from "react-native"
import { RootStackParamList } from "../../App";
import pokemonService from "../../api/pokemonService";
import { IPokemonMove } from "../../dto/IPokemonMove";
import { useMemo } from "react";
import PokemonImage from "../../components/PokemonImage";
import ErrorScreen from "../../components/ErrorScreen";
import { IPokemonDetail } from "../../dto/IPokemonDetail";


const PokemonDetails = ({route}: {route: RouteProp<RootStackParamList, 'PokemonDetails'> }) => {
  const { pokemonId } = route.params;
  const { data: pokemon, isLoading, isError, refetch } = pokemonService.useGetPokemon(pokemonId!);

  const moves: {name: string, level_learned_at: number}[] = useMemo(() => 
    pokemon?.moves?.filter((move: IPokemonMove) => move.version_group_details
      .some((detail: IPokemonDetail) => detail.move_learn_method.name === 'level-up' && detail.level_learned_at > 0))
      .slice(0, 4)
      .map((move: IPokemonMove) => ({
        name: move.move.name,
        level_learned_at: move?.version_group_details?.find?.((detail: IPokemonDetail) => detail.move_learn_method.name === 'level-up')?.level_learned_at
      })) ?? [], [pokemon])

  if(isError) {
    return <ErrorScreen refetch={refetch} text='Nie byliśmy w stanie załadować pokemona' />
  }

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  return (

    <View style={styles.container}>
      <View style={styles.imageWrapper}>
      <PokemonImage style={styles.image} pokemonId={pokemonId} />
      </View>
      <Text style={styles.pokemonName}>{pokemon?.name}</Text>

      <Text style={styles.movesHeader}>Podstawowe ataki</Text>
      <View style={styles.moves}>

        {moves?.map((move) => {
          return (
            <Text key={move.name} style={styles.move}>{move.name}</Text>
          )
        })}
      </View>
    </View>
  )
}


const styles = StyleSheet.create({

  centered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  container: {
    flexDirection: 'column',
    alignItems: 'center'
  },

  imageWrapper: {
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    height: 200,
  },

  image: {
    width: 200,
    height: 200,
    borderRadius: 10,
  },

  pokemonName: {
    fontSize: 24,
    fontWeight: '500'
  },

  movesHeader: {
    textAlign: 'left',
    width: '100%',
    padding: 20,
    fontSize: 18
  },

  moves: {
    margin: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    rowGap: 10,


    
  },

  move: {
    fontSize: 16,
    textAlign: 'center',
    width: '50%'
  }
})

export default PokemonDetails;