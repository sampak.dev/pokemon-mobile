import axios from 'axios';
import { useEffect, useMemo, useState } from 'react';
import { FlatList, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, RefreshControl, Button, ActivityIndicator } from 'react-native';
import PokemonCard from '../../components/PokemonCard';
import { IPokemonResult } from '../../dto/IPokemonResult';
import pokemonService from '../../api/pokemonService';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../App';
import ErrorScreen from '../../components/ErrorScreen';

const PokemonList = () => {
  type PokemonListNavigationProp = StackNavigationProp<RootStackParamList, 'PokemonList'>;
  const navigation = useNavigation<PokemonListNavigationProp>();
  const { data, refetch, remove, fetchNextPage, isError, isFetching, isFetchingNextPage, hasNextPage } = pokemonService.useGetList();
  const pokemons = useMemo(() => data?.pages.map(page => page.results).flat(), [data]);

  


  const handlePressPokemon = (pokemon: IPokemonResult) => {
    const pokemonId = pokemon.url.split("/").at(-2) ?? '' 
    navigation.navigate('PokemonDetails', { pokemonId });
  };



  if(isError) {
    return <ErrorScreen refetch={refetch} text='Nie byliśmy w stanie załadować listy' />
  }

  return (
    <View style={styles.container}>
      <FlatList
        onEndReached={() => {
          if (isFetchingNextPage || !hasNextPage) return;
          fetchNextPage()
        }}
        onEndReachedThreshold={0.5}
        refreshControl={
          <RefreshControl refreshing={isFetching || isFetchingNextPage} onRefresh={() => {
            remove();
            refetch();
          }} />
        }
        data={pokemons}
        renderItem={({item}) => (
          <TouchableOpacity onPress={() => handlePressPokemon(item)}>

            <PokemonCard pokemon={item} />
          </TouchableOpacity>
      )}
        keyExtractor={(item: IPokemonResult) => item?.name}
        ListFooterComponent={() => 
          isFetchingNextPage ? <ActivityIndicator size="large" color="#0000ff" /> : null
        }
      />

    </View>
  )

  
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
  card: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 10,
    margin: 10,
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  info: {
    flex: 1,
    paddingHorizontal: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  location: {
    fontSize: 14,
    color: '#666',
  },
  price: {
    fontSize: 16,
    color: '#000',
  },
  button: {
    backgroundColor: '#58CC02',
    padding: 5,
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
  },
});

export default PokemonList;