export const EViews = {
  POKEMON_LIST: 'PokemonList',
  POKEMON_DETAILS: 'PokemonDetails'
} as const

export type ViewsKeys = typeof EViews[keyof typeof EViews]