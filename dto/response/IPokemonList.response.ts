import { IPokemonResult } from "../IPokemonResult";

export interface IPokemonListResponse {
  count: number;
  next: string | null;
  previous: string | null;
  results: IPokemonResult[]
}