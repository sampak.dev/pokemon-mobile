export interface IPokemonDetail {
  move_learn_method: {
    name: string
  }
  level_learned_at: number;
  
}