import { IPokemonDetail } from "./IPokemonDetail";

export interface IPokemonMove {
  name: string;
  url: string;
  version_group_details: IPokemonDetail[]

  move: {
    name: string
  }
}