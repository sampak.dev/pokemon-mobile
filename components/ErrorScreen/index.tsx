import { StyleSheet, Image, Text, View, Button } from 'react-native';
import { FC, Props } from './typings';

const ErrorScreen: FC<Props> = ({text, refetch}) => {
  return (
    <View style={styles.container}>

      <Image style={styles.image} source={require('../../assets/sad.png')} />
      <Text style={styles.text}>{text}</Text>
      <Button onPress={() => {
        console.log("klik")
        refetch();
      }} title='Spróbuj ponownie' />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: 200,
    height: 200
  },

  text: {
    fontSize: 18,
    paddingTop: 20,
    paddingBottom: 20
  },


});


export default ErrorScreen;