import { StyleSheet, View, Text } from "react-native";
import { FC, Props } from "./typings";
import PokemonImage from "../PokemonImage";

const PokemonCard: FC<Props> = ({pokemon}) => {
  const link = pokemon.url;
  const pokemonId = link.split('/').at(-2);
  

  return <View style={styles.card}>

    <PokemonImage style={styles.image} pokemonId={pokemonId} />
    <View style={styles.info}>
      <Text style={styles.title}>{pokemon?.name}</Text>
      <Text style={styles.id}>#{pokemon?.url?.split('/').filter(Boolean).pop()}</Text>
    </View>

  </View>
}

const styles = StyleSheet.create({

  card: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 10,
    margin: 10,
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  info: {
    flex: 1,
    paddingHorizontal: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
 
  id: {
    fontSize: 16,
    color: '#000',
  },

});

export default PokemonCard;