import { IPokemonResult } from '../../dto/IPokemonResult';

export type { FC } from 'react';

export interface Props {
  pokemon: IPokemonResult
}