import { ImageStyle, StyleProp } from 'react-native';

export type { FC } from 'react';

export interface Props {
  pokemonId: string;
  style?: StyleProp<ImageStyle>;

}