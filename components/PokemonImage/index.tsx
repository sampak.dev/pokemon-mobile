import { Image } from "react-native";
import { FC, Props } from "./typings"

const PokemonImage: FC<Props> = ({pokemonId, style}) => {
  return <Image 
  style={style && style }
  source={{ uri: `${process.env.EXPO_PUBLIC_IMAGES_STORAGE_URL}/${pokemonId}.png` }}
  />
}

export default PokemonImage;